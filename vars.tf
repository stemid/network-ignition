variable "hostname" {
  type = string
}

variable "connections" {
  type = list(object({
    method = string,
    interface = string,
    hostname = string,
    type = optional(string, "ethernet"),
    ip = optional(string, ""),
    suffix = optional(string, ""),
    gateway = optional(string, ""),
    nameservers = optional(list(string), []),
    dns_search = optional(string, "")
  }))
}
