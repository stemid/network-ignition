output "config" {
  value = data.ignition_config.network
}

# Provide all ignition objects as output so a calling module can put together
# its own ignition_config that results in smaller total size.
output "ignition_user" {
  value = []
}

output "ignition_file" {
  value = concat(
    [for f in data.ignition_file.connection : f],
    [data.ignition_file.hostname]
  )
}

output "ignition_link" {
  value = []
}

output "ignition_systemd_unit" {
  value = []
}

output "ignition_directory" {
  value = []
}
