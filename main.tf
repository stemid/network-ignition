data "ignition_file" "connection" {
  for_each = {
    for index, conn in var.connections : index => conn
  }

  path = "/etc/NetworkManager/system-connections/${each.value.hostname}.nmconnection"
  mode = 384
  content {
    content = templatefile("${path.module}/templates/interface.nmconnection", {
      conn = each.value
    })
  }
}

data "ignition_file" "hostname" {
  path = "/etc/hostname"
  mode = 420
  overwrite = true
  content {
    content = "${var.hostname}"
  }
}

data "ignition_config" "network" {
  files = concat([
    data.ignition_file.hostname.rendered,
  ], [for file in data.ignition_file.connection : file.rendered])
}
